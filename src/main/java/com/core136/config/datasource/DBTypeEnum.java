package com.core136.config.datasource;
/**
 * 
 * <p>	Title: DBTypeEnum </p>
 * <p>	Description: 枚举  </p>
 * @author	曹凯
 * @date	2020年7月31日下午2:11:45
 * @version 1.0
 */
public enum DBTypeEnum {
	
	MASTER,
	SLAVE1,
	SLAVE2,
	SLAVE3;
	

}
